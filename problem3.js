const sol3 = (inventory) => {
    return inventory.sort((a, b) => {
        return a.car_model.localeCompare(b.car_model);
    });
};
module.exports = sol3;
